﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class v_open_alarms
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public DateTime? date_in { get; set; }
        public int? idm_raw { get; set; }
        public string? id_unique { get; set; }
        [Display(Name = "Устройство")]
        public int? idm_device { get; set; }

        //public int? ref { get; set; }
        [Display(Name = "Имя устр-ва")]
        public string? device_name { get; set; }
        [Display(Name = "Описание")]
        public string? alarm_text { get; set; }
        [Display(Name = "Класс аварии")]
        public string? alarm_class { get; set; }
        [Display(Name = "Статус")]
        public int? status { get; set; }
        [Display(Name = "Приоритет")]
        public int? idm_priotity { get; set; }
        //public link_devices_priorities link_devices_priorities { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime? date_start { get; set; }

        [Display(Name = "Дата устранения")]
        public DateTime? date_end { get; set; }
        [Display(Name = "Открыто, сек.")]
        public int? sec_live_time { get; set; }
        [Display(Name = "Открыто, мин.")]
        public int? min_live_time_last { get; set; }
        [Display(Name = "Открыто, дн.")]
        public int? day_live_time_last { get; set; }
        [Display(Name = "ГУИД")]
        public string? guid { get; set; }
        public int? send_timeout { get; set; }
        public int? reset_timeout { get; set; }
        public int? itsm_send_status { get; set; }
        [Display(Name = "Дата отправки")]
        public DateTime? itsm_date_send { get; set; }
        [Display(Name = "Дата доставки")]
        public DateTime? itsm_date_delivery { get; set; }
        [Display(Name = "Состояние")]
        public string? itsm_status_description { get; set; }
        public string? itsm_status_icon { get; set; }
        [Display(Name = "Дата открытия")]
        public string? date_start_ro { get; set; }
        [Display(Name = "Дата закрытия")]
        public string? date_end_ro { get; set; }

        [Column("number")]
        [Display(Name = "Вн. Номер")]
        public int? number { get; set; }
        public string? status_description { get; set; }
        public string? status_icon { get; set; }

        [Display(Name = "Время жизни")]
        public string? live_time { get; set; }
        [Display(Name = "Объект")]
        public string? object_name { get; set; }
        public int idm_object { get; set; }
        [Display(Name = "Модель")]
        public string? device_model { get; set; }
        [Display(Name = "Id устройства")]
        public string? device_id { get; set; }
        public string? unit_name { get; set; }



        //public link_devices_priorities link_devices_priorities { get; set; }

    }
}
