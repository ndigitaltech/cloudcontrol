﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class recipient_objects_ref
    {
        public int idm_recipient { get; set; }
        public int idm_object { get; set; }
        
    }
}
