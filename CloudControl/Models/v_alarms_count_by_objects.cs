﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class v_alarms_count_by_objects
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int _count { get; set; }
        public int? object_id { get; set; }
        public string? description { get; set; }
        public DateTime? date_in { get; set; }
       
    }
}
