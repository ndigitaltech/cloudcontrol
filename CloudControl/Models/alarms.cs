﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class alarms
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }        
        public DateTime? date_in { get; set; }
        public int? idm_raw { get; set; }
        public string? id_unique { get; set; }
        [Display(Name = "Устройство")]
        public int? idm_device { get; set; }

        //public int? ref { get; set; }
        [Display(Name = "Имя устр-ва")]
        public string? device_name { get; set; }
        public string? alarm_text { get; set; }
        public string? alarm_class { get; set; }
        public int? status { get; set; }
        public int? idm_priotity { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime? date_start { get; set; }

        [Display(Name = "Дата устранения")]
        public DateTime? date_end { get; set; }
        public int? sec_live_time { get; set; }
        public string? guid { get; set; }
        public int? itsm_send_status { get; set; }
        public DateTime? itsm_date_send { get; set; }
        public DateTime? itsm_date_delivery { get; set; }


        //public link_devices_priorities link_devices_priorities { get; set; }


    }
}
