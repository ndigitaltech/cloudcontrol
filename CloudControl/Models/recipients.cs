﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class recipients
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Display(Name = "Получатель")]
        public string? recipient { get; set; }
        public string? macAddr { get; set; }
        [Display(Name = "Включен")]
        public bool? is_active { get; set; }
        [Display(Name = "СМС")]
        
        public bool? is_sms { get; set; }
        [Display(Name = "Whatsapp")]
        
        public bool? is_whatsapp { get; set; }
        [Display(Name = "Telegramm")]
        
        public bool? is_telegram { get; set; }
        [Display(Name = "E-mail")]
        
        public bool? is_email { get; set; }        
        public string? idm_client { get; set; }        
    }
}
