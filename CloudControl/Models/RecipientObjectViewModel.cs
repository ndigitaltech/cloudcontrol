﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class RecipientObjectViewModel
    {
        public string idm_recipient { get; set; }
        public int idm_object { get; set; }
        public string object_description { get; set; }
        public bool selected { get; set; }



    }
}
