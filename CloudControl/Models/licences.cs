﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class licences
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Display(Name = "Начало действия")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_in { get; set; }
        [Display(Name = "Срок действия")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_expired { get; set; }
        public string idm_user { get; set; }

        [Display(Name = "Устройства")]
        public int devices { get; set; }
        [Display(Name = "Объекты")]
        public int objects { get; set; }

        [Display(Name = "Получатели")]
        public int recipients { get; set; }
        [Display(Name = "Номер лицензии")]
        public string number { get; set; }
        
    }
}
