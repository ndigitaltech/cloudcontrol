﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class mail_messages
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? date_in { get; set; }
        public int? idm_channel { get; set; }
        public string? mail_to { get; set; }
        public string? mail_reply { get; set; }
        public string? mail_title { get; set; }
        public string? mail_body { get; set; }
        public string? mail_files { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? date_sending { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? date_expired { get; set; }
        public DateTime? date_sended { get; set; }
        public int status { get; set; }
        public Guid? guid { get; set; }
        public int? idm_request { get; set; }
    }
}
