﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class UserRolesViewModel
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string? NewPassword { get; set; }
        
        public string Email { get; set; }
        public bool Lockout { get; set; }
        [Display(Name = "Телефон")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Роли")]
        public IEnumerable<string> Roles { get; set; }
        [Display(Name = "Объекты")]
        public IEnumerable<string> ObjectControl { get; set; }

    }
}
