﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class link_devices_priorities
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Display(Name = "Тип")]
        public int? idm_device_type { get; set; }
        [Display(Name = "Приоритет")]
        public string? priority { get; set; }
        [Display(Name = "Код")]
        public int? idm_priority { get; set; }
        [Display(Name = "Таймаут самоустранения, сек.")]
        public int? sec_self_removal { get; set; }
        [Display(Name = "ID Шаблона ")]
        public string? tmpl_name { get; set; }
        [Display(Name = "Таймаут сброса, сек.")]
        public int? sec_reset { get; set; }
    }
}
