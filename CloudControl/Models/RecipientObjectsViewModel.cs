﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class RecipientObjectsViewModel
    {

        public int id { get; set; }
        [Display(Name = "Получатель")]
        public string? recipient { get; set; }        
        [Display(Name = "Включен")]
        public bool? is_active { get; set; }
        [Display(Name = "СМС")]
        public bool? is_sms { get; set; }
        [Display(Name = "Whatsapp")]
        public bool? is_whatsapp { get; set; }
        [Display(Name = "Telegramm")]
        public bool? is_telegram { get; set; }
        [Display(Name = "E-mail")]
        public bool? is_email { get; set; }

        [Display(Name = "Объекты")]
        public IEnumerable<string> ObjectControl { get; set; }
        public int? all { get; set; }
        public int? sub { get; set; }
        public string? listObjects { get; set; }


    }
}
