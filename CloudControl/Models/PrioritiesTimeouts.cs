﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class PrioritiesTimeouts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int? num { get; set; }
        public string? name { get; set; }        
        public int? send_timeout { get; set; }        
        public int? reset_timeout { get; set; }        
        public string? tmpl_ID { get; set; }        
    }
}
