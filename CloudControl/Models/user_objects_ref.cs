﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class user_objects_ref
    {
        public string idm_user { get; set; }
        public int idm_object { get; set; }
        
    }
}
