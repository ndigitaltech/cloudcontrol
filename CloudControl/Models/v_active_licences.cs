﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class v_active_licences
    {
        
        public string id { get; set; }
        [Display(Name = "Пользователь")]
        public string Email { get; set; }
        [Display(Name = "Начало действия")]        
        public DateTime? date_in { get; set; }
        [Display(Name = "Срок действия")]        
        public DateTime? date_expired { get; set; }
        [Display(Name = "Устройства")]
        public int? cur_devices { get; set; }
        [Display(Name = "Лимит устройств")]
        public int? d_limit { get; set; }
        [Display(Name = "Доступно устройств")]
        public int d_avail { get; set; }

        [Display(Name = "Объекты")]
        public int? cur_object { get; set; }

        [Display(Name = "Лимит объектов")]
        public int? o_limit { get; set; }
        [Display(Name = "Доступно объектов")]
        public int o_avail { get; set; }

        [Display(Name = "Получатели")]
        public int? cur_recipients { get; set; }
        [Display(Name = "Лимит получателей")]
        public int? r_limit { get; set; }
        [Display(Name = "Доступно получателей")]
        public int r_avail { get; set; }

        [Display(Name = "Номер лицензии")]
        public string? number { get; set; }
        
    }
}
