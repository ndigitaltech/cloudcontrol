﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class devices
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Display(Name = "Объект")]
        public int? idm_object { get; set; }
        [Display(Name = "Внешний IP адрес")]
        public string? ip { get; set; }
        [Display(Name = "Описание")]
        public string? descr { get; set; }
        public string? url { get; set; }
        public string? login { get; set; }
        [DataType(DataType.Password)]
        public string? password { get; set; }
        [Display(Name = "Тип")]
        public int? idm_type { get; set; }

        [Display(Name = "Активность")]
        //[UIHint("Boolean")]
        [DefaultValue(1)]
        public int? is_enable { get; set; }

        [Display(Name = "Размер пакета аварий")]        
        [DefaultValue(100)]
        public int? length { get; set; }

        [Display(Name = "Период опроса")]
        [DefaultValue(180)]
        public int? sec_upload { get; set; }
        [Display(Name = "Последн. данные")]
        public DateTime? date_last_upload { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string? guid { get; set; }
        [Display(Name = "IP устройства в ЛВС")]
        public string? ip_device { get; set; }
        [Display(Name = "Серийный номер")]
        public string? serial { get; set; }
        [Display(Name = "MAC адрес")]
        public string? mac_address { get; set; }


        public devices()
        {
            length = 100;
            is_enable = 1;
            sec_upload = 180;

        }
    }

    
}
