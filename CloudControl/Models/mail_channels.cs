﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CloudControl.Models
{
    public class mail_channels
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_in { get; set; }
        public string channel_name { get; set; }
        public string server_host { get; set; }
        public int server_port { get; set; }
        public int server_ssl { get; set; }
        public string mail_from { get; set; }
        public string mail_from_password { get; set; }
        public string mail_from_title { get; set; }
        public int disable { get; set; }
    }
}
