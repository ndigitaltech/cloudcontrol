﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace CloudControl.Controllers
{
    [Authorize]
    public class RecipientController : Controller
    {
        private readonly ApplicationDbContext _db;


        public RecipientController(ApplicationDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View("Index2");
        }

        [HttpGet]
        public async Task<object> GetList()
        {
            // сделать проверку на наличие прав админа
            var isAdmin = User.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == "Admin");
            var isClientAdmin = User.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == "ClientAdmin");



            if ((!isClientAdmin) && (!isAdmin))
            {
                return BadRequest(new { msg = "У Вас нет доступа."});
            }

            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();

            var allowed_objects = new List<int>();

            if (isClientAdmin)
            {
                allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();
            }
            if (isAdmin)
            {
                allowed_objects = _db.user_objects_ref.Select(u => u.idm_object).ToList();
            }

            int all_obj = allowed_objects.Count();


            var all_recipients = new List<recipients>();
            if (isClientAdmin)
            {
                all_recipients = _db.recipients.Where(r => r.idm_client == currentUserID).OrderBy(r => r.recipient).ToList();
            }
            if (isAdmin)
            {
                all_recipients = _db.recipients.OrderBy(r => r.recipient).ToList();
            }

            
            var recipientObjectsViewModel = new List<RecipientObjectsViewModel>();
            foreach (recipients recipient in all_recipients)
            {
                var thisViewModel = new RecipientObjectsViewModel();

                string listObjects = "";
                foreach(string obj in await GetObjectsControl(recipient)) {
                    listObjects = listObjects + obj + "; ";
                }

                thisViewModel.id = recipient.id;
                thisViewModel.recipient = recipient.recipient;
                thisViewModel.is_active = recipient.is_active == null ? false : true;
                thisViewModel.is_sms = recipient.is_sms == null ? false : true; 
                thisViewModel.is_whatsapp = recipient.is_whatsapp == null ? false : true; 
                thisViewModel.is_telegram = recipient.is_telegram == null ? false : true; 
                thisViewModel.is_email = recipient.is_email == null ? false : true; 
                //thisViewModel.ObjectControl = await GetObjectsControl(recipient);
                thisViewModel.listObjects = listObjects;
                thisViewModel.all = all_obj;
                thisViewModel.sub = await GetObjectsControlCount(recipient); ;

                recipientObjectsViewModel.Add(thisViewModel);
            }
            return recipientObjectsViewModel;
            //return View();
        }

        /// <summary>
        /// Генерация формы отображения списка объектов по получателю
        /// </summary>
        /// <param name="recipId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Objects(int recipId)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();

            ViewBag.RecipId = recipId;
            var recipient = _db.recipients.First(u => u.id == recipId);
            if (recipient == null)
            {
                ViewBag.ErrorMessage = $"Recipient with Id = {recipId.ToString()} cannot be found";
                return View("NotFound");
            }
            ViewBag.RecipNumber = recipient.recipient;
            var model = new List<RecipientObjectViewModel>();
            foreach (var obj in _db.ObjectControl.Where(o => allowed_objects.Contains(o.id)).OrderBy(o => o.description))
            {
                var recipientObjectViewModel = new RecipientObjectViewModel
                {
                    idm_object = obj.id,
                    object_description = obj.description
                };


                //if (await _userManager.IsInRoleAsync(user, role.Name))
                if (_db.recipient_objects_ref.Any(r => r.idm_recipient == recipient.id && r.idm_object == obj.id))
                {
                    recipientObjectViewModel.selected = true;
                }
                else
                {
                    recipientObjectViewModel.selected = false;
                }
                model.Add(recipientObjectViewModel);
            }
            return PartialView(model);
        }
        /// <summary>
        /// Сохранение изменений связи получателя и объектов
        /// </summary>
        /// <param name="model"></param>
        /// <param name="recipId"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Objects(List<RecipientObjectViewModel> model, int recipId)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();

            var recip = _db.recipients.Where(u => u.id == recipId).First();
            if (recip == null)
            {
                return View();
            }
            var objects = _db.recipient_objects_ref.Where(r => r.idm_recipient == recip.id && allowed_objects.Contains(r.idm_object));
            // всё удаляю
            foreach (var obj in objects)
            {
                _db.recipient_objects_ref.Remove(obj);

            }
            _db.SaveChanges();
            //создаю только пришедший список
            foreach (var new_row in model)
            {
                if (new_row.selected && allowed_objects.Contains(new_row.idm_object))
                {
                    //var new_row = new IdentityUserRole() { RoleId = new_role.RoleId, UserId = user.Id };

                    _db.recipient_objects_ref.Add(new recipient_objects_ref { idm_object = new_row.idm_object, idm_recipient = recip.id });

                }
            }
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Старый метод получения списка получателей сообщений
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[HttpGet]
        //public object Get(int? id)
        //{
        //    var list = new List<recipients>();
        //    if (id == null)
        //    {
        //         list = _db.recipients.ToList();
        //    }
        //    else
        //    {
        //         list = _db.recipients.Where(o => o.id == id).ToList();
        //    }
            
        //    return list;
        //}

        [HttpPost]
        public IActionResult Create(string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var newRow = new recipients();
            if (values != null)
            {
                JsonConvert.PopulateObject(values, newRow);
                newRow.idm_client = currentUserID;
                _db.recipients.Add(newRow);
                _db.SaveChanges();
            }
            return Ok(newRow);
        }


        [HttpPut]
        public IActionResult Update(int key, string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var newData = _db.recipients.First(o => o.id == key);
            JsonConvert.PopulateObject(values, newData);
            if ((newData.idm_client == currentUserID) || (User.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == "Admin")))
            {
                _db.SaveChanges();
                return Ok(newData);
            } else
            {
                return BadRequest(new { msg = "У Вас нет прав на изменение" });
            }
            
        }


        [HttpDelete]
        public void Delete(int key)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var row = _db.recipients.First(o => o.id == key);
            if ((row.idm_client == currentUserID) || (User.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == "Admin")))
            {
                _db.recipients.Remove(row);
                _db.SaveChanges();
            }
             
        }

        
        private async Task<List<string>> GetObjectsControl(recipients recipient)
        {
            List<int> objects_id = _db.recipient_objects_ref.Where(r => r.idm_recipient == recipient.id).Select(r => r.idm_object).ToList();
            return new List<string>(_db.ObjectControl.Where(r => objects_id.Contains(r.id)).Select(r => r.description).ToList());
        }

        private async Task<int> GetObjectsControlCount(recipients recipient)
        {
            int subscribed = _db.recipient_objects_ref.Where(r => r.idm_recipient == recipient.id).Select(r => r.idm_object).Count();            

            return subscribed;
        }

    }
}
