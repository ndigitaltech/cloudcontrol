﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace CloudControl.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleManagerController : Controller
    {
        //private readonly RoleManager<IdentityRole> _roleManager;
        //public RoleManagerController(RoleManager<IdentityRole> roleManager)
        //{
        //    _roleManager = roleManager;
        //}
        //public async Task<IActionResult> Index()
        //{
        //    var roles = _roleManager.Roles.ToList();
        //    return View(roles);
        //}

        private readonly ApplicationDbContext _db;
        public RoleManagerController(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<IActionResult> Index()
        {
            var roles = _db.Roles.ToList();
            return View(roles);
        }

        //[HttpPost]
        //public async Task<IActionResult> AddRole(string roleName)
        //{
        //    if (roleName != null)
        //    {
        //        _db.Create(new IdentityRole(roleName.Trim()));
        //    }
        //    return RedirectToAction("Index");
        //}
    }
}
