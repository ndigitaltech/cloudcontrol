﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using CloudControl.Models;
using CloudControl.Data;
using Newtonsoft.Json;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
//using Microsoft.IdentityModel;




namespace CloudControl.Controllers
{

    public class jsonNewUser : UserRolesViewModel
    {
        public string NewPassword { get; set; }
    }
    [Authorize(Roles ="Admin")]
    public class AdminController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        //private readonly RoleManager<IdentityRole> _roleManager;
        //public UserRolesController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        //{
        //    _roleManager = roleManager;
        //    _userManager = userManager;
        //}
        private readonly ApplicationDbContext _db;
        public AdminController(ApplicationDbContext db, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _signInManager = signInManager;
            _userManager = userManager;
            
        }
        public async Task<IActionResult> Index()
        {
            if (!User.HasClaim(
                        c => c.Type == ClaimTypes.Role
                        && c.Value == "Admin"))
            {
                RedirectToAction("/");
            }
            //IdentityUserRole
            var users = _db.Users.ToList();
            var userRolesViewModel = new List<UserRolesViewModel>();
            foreach (Microsoft.AspNetCore.Identity.IdentityUser user in users)
            {
                var thisViewModel = new UserRolesViewModel();
                thisViewModel.UserId = user.Id;
                thisViewModel.Email = user.Email;
                thisViewModel.FirstName = user.UserName;                
                thisViewModel.Lockout = user.LockoutEnabled;                
                thisViewModel.Roles = await GetUserRoles(user);
                thisViewModel.ObjectControl = await GetObjectsControl(user);
                thisViewModel.PhoneNumber = user.PhoneNumber;
                thisViewModel.NewPassword = "";
                
                userRolesViewModel.Add(thisViewModel);
            }
            return View("Index2",userRolesViewModel);
        }
        private async Task<List<string>> GetUserRoles(IdentityUser user)
        {
            List<string> roles_id = _db.UserRoles.Where(r => r.UserId == user.Id).Select(r => r.RoleId).ToList();
            return new List<string>(_db.Roles.Where(r => roles_id.Contains(r.Id) ).Select(r => r.Name).ToList());
        }

        private async Task<List<string>> GetObjectsControl(IdentityUser user)
        {
            List<int> objects_id = _db.user_objects_ref.Where(r => r.idm_user == user.Id).Select(r => r.idm_object).ToList();
            return new List<string>(_db.ObjectControl.Where(r => objects_id.Contains(r.id)).Select(r => r.description).ToList());
        }

        public async Task<IActionResult> Manage(string userId)
        {
            ViewBag.userId = userId;
            var user = _db.Users.First(u => u.Id == userId);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                return View("NotFound");
            }
            ViewBag.UserName = user.UserName;
            var model = new List<ManageUserRolesViewModel>();
            foreach (var role in _db.Roles)
            {
                var userRolesViewModel = new ManageUserRolesViewModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };


                //if (await _userManager.IsInRoleAsync(user, role.Name))
                if (_db.UserRoles.Any(r => r.RoleId == role.Id && r.UserId == user.Id))
                {
                    userRolesViewModel.Selected = true;
                }
                else
                {
                    userRolesViewModel.Selected = false;
                }
                model.Add(userRolesViewModel);
            }
            return PartialView(model);
        }


        [HttpPost]
        public  IActionResult Manage(List<ManageUserRolesViewModel> model, string userId)
        {
            var user = _db.Users.Where(u => u.Id == userId).First();
            if (user == null)
            {
                return View();
            }
            var roles = _db.UserRoles.Where( r => r.UserId == user.Id);
            // всё удаляю
            foreach(var role in roles)
            {
                _db.UserRoles.Remove(role);
                 
            }
            _db.SaveChanges();
            //создаю только пришедший список
            foreach (var new_role in model)
            {
                if (new_role.Selected)
                {
                    //var new_row = new IdentityUserRole() { RoleId = new_role.RoleId, UserId = user.Id };

                    _db.UserRoles.Add(new IdentityUserRole<string> { RoleId = new_role.RoleId, UserId = user.Id});
                     
                }
            }
            _db.SaveChanges();
            
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult UserOff(string userId)
        {
            var user = _db.Users.Where(u => u.Id == userId).First();
            
            if (user.LockoutEnabled)
            {
                user.LockoutEnabled = false;
            } else
            {
                user.LockoutEnabled = true;
            }            
            _db.SaveChanges();
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public object Create([FromForm] string UserName, [FromForm] string Email,[FromForm] string PhoneNumber, [FromForm] string NewPassword)
        {
            //var json = new jsonNewUser();
            //JsonConvert.PopulateObject(data, json);            

            var user = new IdentityUser { 
                UserName = Email, 
                Email = Email,
                PhoneNumber = PhoneNumber,
                EmailConfirmed = true
                //PasswordHash = WebEncoders.Base64UrlEncode(System.Text.Encoding.UTF8.GetBytes(NewPassword))            
            };
            //_signInManager.SignInAsync(user, isPersistent: false);
            var result = _userManager.CreateAsync(user, NewPassword);

            return result;
        }

        [HttpPost]
        public async Task<object> Update([FromForm] string UserId, [FromForm] string Email, [FromForm] string PhoneNumber, [FromForm] string NewPassword)
        {
            //var json = new jsonNewUser();
            //JsonConvert.PopulateObject(data, json);            

            IdentityResult res = new IdentityResult();
            //res.Succeeded = true;

            var userone = _userManager.Users.Where(u => u.Id == UserId).FirstOrDefault();            
            if (PhoneNumber != null)
            {
                 res = await _userManager.SetPhoneNumberAsync(userone, PhoneNumber);
                if (!res.Succeeded)
                {
                    return res;
                }
            }

            if (Email != null)
            {
                
                res = await _userManager.SetEmailAsync(userone, Email);
                if (!res.Succeeded)
                {
                    return res;
                }
                res = await _userManager.SetUserNameAsync(userone, Email.ToLower());
                if (!res.Succeeded)
                {
                    return res;
                }

                var usernew = _userManager.Users.Where(u => u.Id == UserId).FirstOrDefault();
                usernew.EmailConfirmed = true;
                string confirmToken = await _userManager.GenerateChangeEmailTokenAsync(usernew, Email);
                res = await _userManager.ConfirmEmailAsync(usernew, confirmToken);

            }


            if (NewPassword != null)
            {
                string resetToken = await _userManager.GeneratePasswordResetTokenAsync(userone);
                 res = await _userManager.ResetPasswordAsync(userone, resetToken, NewPassword);
                if (!res.Succeeded)
                {
                    return res;
                }
            }


            return res;


            //_signInManager.SignInAsync(user, isPersistent: false);
            //var result = _userManager.UpdateAsync(user, NewPassword);

            //return RedirectToAction("Index");
            //return true;
        }

        //[HttpGet]
        public object MyClaims()
        {
            List<object> claims = new List<object>();
            var userid = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();

            foreach (var claim in User.Claims.ToList())
            {
                claims.Add(new
                {
                    type = claim.Type,
                    value = claim.Value

                });
            }
            return claims;
        }


        public object getUserId()
        {

            var userid = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();

            return userid;
        }

        public async Task<IActionResult> Objects(string userId)
        {
            ViewBag.userId = userId;
            var user = _db.Users.First(u => u.Id == userId);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                return View("NotFound");
            }
            ViewBag.UserName = user.UserName;
            var model = new List<UserObjectsViewModel>();
            foreach (var obj in _db.ObjectControl)
            {
                var UserObjectsViewModel = new UserObjectsViewModel
                {
                    idm_object = obj.id,
                    object_description = obj.description
                };


                //if (await _userManager.IsInRoleAsync(user, role.Name))
                if (_db.user_objects_ref.Any(r => r.idm_user == user.Id && r.idm_object == obj.id))
                {
                    UserObjectsViewModel.selected = true;
                }
                else
                {
                    UserObjectsViewModel.selected = false;
                }
                model.Add(UserObjectsViewModel);
            }
            return PartialView(model);
        }

        [HttpPost]
        public IActionResult Objects(List<UserObjectsViewModel> model, string userId)
        {
            var user = _db.Users.Where(u => u.Id == userId).First();
            if (user == null)
            {
                return View();
            }
            var objects = _db.user_objects_ref.Where(r => r.idm_user == user.Id);
            // всё удаляю
            foreach (var obj in objects)
            {
                _db.user_objects_ref.Remove(obj);

            }
            _db.SaveChanges();
            //создаю только пришедший список
            foreach (var new_row in model)
            {
                if (new_row.selected)
                {
                    //var new_row = new IdentityUserRole() { RoleId = new_role.RoleId, UserId = user.Id };

                    _db.user_objects_ref.Add(new user_objects_ref { idm_object = new_row.idm_object, idm_user = user.Id });

                }
            }
            _db.SaveChanges();

            return RedirectToAction("Index");
        }



    }
}
