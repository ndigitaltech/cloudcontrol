﻿using CloudControl.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace CloudControl.Controllers
{
    [Authorize]

    public class report1
    {
        public string bar_name { get; set; }
        
        public int? open { get; set; }
        
        public int? close { get; set; }
        
        public int? error { get; set; }
        
        public int? twice { get; set; }
    }

    public class report3
    {
        public string date_in { get; set; }

        public int? _count { get; set; }
        
    }


    public class DashboardController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;

        public DashboardController(ApplicationDbContext db, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        /// <summary>
        /// аварии по объектам и статусам 
        /// </summary>
        /// <returns></returns>
        public object Report1()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());
            var raw_data = _db.v_alarms_by_objects.Where(o => restricted_objects.Contains((int)o.object_id)).ToList();
            var out_data = new List<object>();

            foreach (var obj in raw_data.Select(d => new { d.object_id,d.description }).Distinct())
            {
                var bar_data = new report1() { open = 0, close = 0, error = 0, twice = 0};
                foreach (var record in raw_data.Where(d=> d.object_id == obj.object_id).ToList())
                {
                    if (record.status == 1)
                    {
                        bar_data.open = record._count;
                    }
                    if (record.status == 2)
                    {
                        bar_data.close = record._count;
                    }
                    if (record.status == 3)
                    {
                        bar_data.twice = record._count;
                    }
                    if (record.status == 4)
                    {
                        bar_data.error = record._count;
                    }
                    //bar_data.Add(new { status = record.status_description, count = record._count });
                }
                bar_data.bar_name = obj.description;
                out_data.Add(bar_data);

            }
            return out_data;
        }
        
        public object Report2()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());
            var raw_data = _db.v_alarms_by_objects.Where(o => restricted_objects.Contains((int)o.object_id)).ToList();

            var out_data = new List<object>();

            foreach (var obj in raw_data.Where(d => d.description != null).Select(d => new { d.object_id,d.description }).Distinct())
            {
                Dictionary<object, object> bar_data = new Dictionary<object, object>();
                var options = new Dictionary<string, int>();
                foreach (var record in raw_data.Where(d=> d.object_id == obj.object_id && d.description != null).ToList())
                {                    
                        options[record.line_name] = record._count;
                }
                var json = JsonConvert.SerializeObject(options);
                var from_json = JsonConvert.DeserializeObject<report1>(json);
                from_json.bar_name = obj.description;
                out_data.Add(from_json);
            }
            return out_data;
            
        }

        /// <summary>
        /// отчет динамика по объектам
        /// </summary>
        /// <returns></returns>
        public  object Report3(int? oid )
        {
           // oid = oid == null ? 0 : oid;

            var raw_data = new List<v_alarms_count_by_objects>();
            if (oid == null)
            {
                raw_data  = _db.v_alarms_count_by_objects.ToList();
            } else
            {
                raw_data = _db.v_alarms_count_by_objects.Where(a => a.object_id == (int)oid).ToList();
            }
            

            var out_data = new List<object>();

            foreach (var obj in raw_data.Where(d => d.description != null).Select(d => new { d.date_in }).Distinct())
            {
                Dictionary<object, object> bar_data = new Dictionary<object, object>();
                var options = new Dictionary<string, int>();
                foreach (var record in raw_data.Where(d => d.date_in == obj.date_in && d.description != null).ToList())
                {
                    options["_count"] = record._count;
                }
                
                var json = JsonConvert.SerializeObject(options);
                var from_json = JsonConvert.DeserializeObject<report3>(json);
                from_json.date_in = Convert.ToDateTime(obj.date_in).ToString("dd.MM.yyyy");
                //from_json = obj.description;
                //var tmp = new { date_in = obj.date_in.ToString(), options };
                out_data.Add(from_json);
            }
            return out_data;

        }

        public object Report3Series()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());

            var raw_data = _db.v_alarms_by_objects.Where(o => restricted_objects.Contains((int)o.object_id)).ToList();
            var out_data = new List<object>();

            foreach (var obj in raw_data.Where(d => d.description != null).Select(d => new { d.description }).Distinct())
            {
                
                out_data.Add(new { valueField = obj.description, name = obj.description });
            }
            return out_data;

        }
        /// <summary>
        /// данные для круговой диаграммы
        /// </summary>
        /// <returns></returns>
        public object Report4()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());
            var raw_data = _db.v_alarms_all_by_status.Where(a => restricted_objects.Contains(a.object_id))

                .GroupBy(a => new { a.status, a.status_description, a.color })
                .Select(g => new { 
                        _count = g.Sum(ai => ai._count),
                        status = g.Key.status,
                        status_description = g.Key.status_description,
                        color  = g.Key.color 
                        }
                    )
                
                .ToList();
            var out_data = new List<object>();

            
            foreach (var record in raw_data.ToList())
            {
                out_data.Add(new { status = record.status_description, count = record._count , color = record.color });
            }
            
            return out_data;

        }



    }
}
