﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace CloudControl.Controllers
{
    [Authorize]

    public class AlarmController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;

        public AlarmController(ApplicationDbContext db, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Closed()
        {
            return View();
        }

        [HttpGet]
        public object Get(int? id)
        {
            List<int> restricted_objects = new ObjectController(_db,_signInManager,_userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());

            var list = new List<v_open_alarms>();
            if (id == null)
            {
                list = _db.v_open_alarms.Where(a => a.date_end == null && restricted_objects.Contains(a.idm_object)).OrderByDescending(a => a.sec_live_time).ToList();
            } else
            {
                list = _db.v_open_alarms.Where(a => a.id == id && restricted_objects.Contains(a.idm_object)).ToList();
            }
            return list;
        }

        [HttpGet]
        public object GetClosed()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());

            var list = _db.v_closed_alarms.Where(a => a.date_end != null && restricted_objects.Contains(a.idm_object)).OrderByDescending(a => a.date_start).ToList();
            return list;
        }

        [HttpPost]
        public IActionResult Create(string values)
        {
            var newRow = new alarms();
            if (values != null)
            {
                JsonConvert.PopulateObject(values, newRow);
                _db.alarms.Add(newRow);
                _db.SaveChanges();
            }
            return Ok(newRow);
        }


        [HttpPut]
        public IActionResult Update(int key, string values)
        {
            var newData = _db.alarms.First(o => o.id == key);
            JsonConvert.PopulateObject(values, newData);
            _db.SaveChanges();
            return Ok(newData);
        }


        [HttpDelete]
        public void Delete(int key)
        {
            var row = _db.alarms.First(o => o.id == key);
            _db.alarms.Remove(row);
            _db.SaveChanges();
        }


    }
}
