﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace CloudControl.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ServiceController : Controller
    {
        private readonly ApplicationDbContext _db;


        public ServiceController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(int? id)
        {
            var list = new List<services>();
            if (id == null)
            {
                 list = _db.services.ToList();
            }
            else
            {
                 list = _db.services.Where(o => o.id == id).ToList();
            }
            
            return list;
        }

    }
}
