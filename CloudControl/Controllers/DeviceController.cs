﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace CloudControl.Controllers
{
    [Authorize]
    public class DeviceController : Controller
    {
        private readonly ApplicationDbContext _db;


        public DeviceController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult DeviceType()
        {
            return View();
        }

        public IActionResult Priority()
        {
            return View();
        }

        // устройства
        [HttpGet]
        public object Get(int? id)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();            
            var allowed_devices = _db.user_devices_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_device).ToList();

            if (id == null)
            {
                var list = _db.devices.Where(d => allowed_devices.Contains(d.id)).ToList();
                return list;
            }
            else
            {
                var list = _db.devices.Where(d => allowed_devices.Contains(d.id) && d.id == id).Select(d => new { d.id, d.descr }).ToList();
                return list;
            }

        }

        [HttpPost]
        public IActionResult Create(string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_devices = _db.user_devices_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_device).ToList();

            var allowCreate = User.HasClaim(
                    c => c.Type == ClaimTypes.Role
                    && c.Value == "ClientAdmin");
            var isAdmin = User.HasClaim(
                    c => c.Type == ClaimTypes.Role
                    && c.Value == "Admin");

            if  (((_db.v_active_licences.Where(l => l.id == currentUserID).Select(l => l.d_avail).FirstOrDefault() > 0) && (allowCreate)) || (isAdmin))
            {
                var newRow = new devices();
                if (values != null)
                {
                    JsonConvert.PopulateObject(values, newRow);
                    _db.devices.Add(newRow);
                    _db.SaveChanges();
                }
                

                var link = new user_devices_ref();
                link.idm_device = newRow.id;
                link.idm_user = currentUserID;
                _db.user_devices_ref.Add(link);
                _db.SaveChanges();

                return Ok(newRow);
            } else
            {
                // лимит на объекты исчерпан
                return BadRequest(new { msg = "Лимит устройств для вашей учетной записи исчерпан" });
            }

            
        }


        [HttpPut]
        public IActionResult Update(int key, string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_devices = _db.user_devices_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_device).ToList();

            if (allowed_devices.Contains(key))
            {
                var newData = _db.devices.First(o => allowed_devices.Contains(o.id) && o.id == key);
                JsonConvert.PopulateObject(values, newData);
                _db.SaveChanges();
                return Ok(newData);
            } else
            {
                return BadRequest(new { msg = "У Вас не достаточно прав для редактирования этого устройства" });
            }
            
        }


        [HttpDelete]
        public void Delete(int key)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_devices = _db.user_devices_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_device).ToList();

            if (allowed_devices.Contains(key))
            {
                var row = _db.devices.First(o => allowed_devices.Contains(o.id) && o.id == key);
                _db.devices.Remove(row);
                _db.SaveChanges();
            }
        }

        // типы устройств
        [HttpGet]
        public object TypeGet(int? id)
        {
            
                if (id == null)
                {
                    var list = _db.devices_type.Where(dt => dt.enable).ToList();
                    return list;
                }
                else
                {
                    var list = _db.devices_type.Where(t => t.id == id && t.enable).ToList();
                    return list;
                }
        }

        [HttpPost]
        public IActionResult TypeCreate(string values)
        {
        var isAdmin = User.HasClaim(
                        c => c.Type == ClaimTypes.Role
                        && c.Value == "Admin");
        if (isAdmin)
        {
            var newRow = new devices_type();
            if (values != null)
            {
                JsonConvert.PopulateObject(values, newRow);
                _db.devices_type.Add(newRow);
                _db.SaveChanges();
            }
            return Ok(newRow);
        }
        else
        {
            return BadRequest(new { msg = "Error" });
        }
        }


        [HttpPut]
        public IActionResult TypeUpdate(int key, string values)
        {
            var isAdmin = User.HasClaim(
                        c => c.Type == ClaimTypes.Role
                        && c.Value == "Admin");
            if (isAdmin)
            {
                var newData = _db.devices_type.First(o => o.id == key);
                JsonConvert.PopulateObject(values, newData);
                _db.SaveChanges();
                return Ok(newData);
            }
            else
            {
                return BadRequest(new { msg = "Error" });
            }
            }


        [HttpDelete]
        public void TypeDelete(int key)
        {
            var isAdmin = User.HasClaim(
                           c => c.Type == ClaimTypes.Role
                           && c.Value == "Admin");
            if (isAdmin)
            {
                var row = _db.devices_type.First(o => o.id == key);
                _db.devices_type.Remove(row);
                _db.SaveChanges();
            }
            else
            {
                //return BadRequest(new { msg = "Error" });
            }
            
        }


        // приоритереты 
        [HttpGet]
        public object PriorityGet(int? id)
        {
            //var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            //var allowed_devices = _db.user_devices_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_device).ToList();

            if (id == null)
            {
                var list = _db.link_devices_priorities.ToList();
                return list;
            }
            else
            {
                var list = _db.link_devices_priorities.Where(t => t.id == id).ToList();
                return list;
            }

        }

        [HttpPut]
        public IActionResult PriorityUpdate(int key, string values)
        {
            var isAdmin = User.HasClaim(
                          c => c.Type == ClaimTypes.Role
                          && c.Value == "Admin");
            if (isAdmin)
            {
                var newData = _db.link_devices_priorities.First(o => o.id == key);
                JsonConvert.PopulateObject(values, newData);
                _db.SaveChanges();
                return Ok(newData);
            } else
            {
                return BadRequest(new { msg = "У Вас не достаточно прав" });
            }
        }



    }


}
