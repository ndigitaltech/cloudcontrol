﻿using CloudControl.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Microsoft.AspNetCore.WebUtilities;

using System.Security.Claims;

namespace CloudControl.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _db;
        public HomeController(ApplicationDbContext db, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            List<int> restricted_objects = new ObjectController(_db, _signInManager, _userManager).GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());
            

            List<ObjectControl> objects = _db.ObjectControl.Where(o => o.description != null && _db.v_alarms_count_by_objects.Select(a => a.object_id).Distinct().Contains(o.id)
            && restricted_objects.Contains(o.id)
            ).ToList();

            return View(objects);
        }
        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
