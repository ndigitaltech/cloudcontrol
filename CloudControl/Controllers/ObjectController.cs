﻿using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Data;
using CloudControl.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;




namespace CloudControl.Controllers
{
    [Authorize]
    public class ObjectController : Controller
    {
        
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        //private Task<IdentityUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        private readonly ApplicationDbContext _db;

        public ObjectController(ApplicationDbContext db, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            
            _signInManager = signInManager;
            _userManager = userManager;
            _db = db;
        }
        public IActionResult Index()
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            return View();
        }

        public IActionResult Info()
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();

            List<int> restricted_objects = this.GetUserObjects(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault());


            List<ObjectControl> objects = _db.ObjectControl.Where(o => o.description != null && _db.v_alarms_count_by_objects.Select(a => a.object_id).Distinct().Contains(o.id)
            && restricted_objects.Contains(o.id)
            ).ToList();

            return View(objects);

            //var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            //return View();
        }

        [HttpGet]
        public object Get(int? id)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var list = new List<ObjectControl>();
            var allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();

            if (id == null)
            {
                 list = _db.ObjectControl.Where(o => allowed_objects.Contains(o.id)).ToList();
            }
            else
            {
                 list = _db.ObjectControl.Where(o => allowed_objects.Contains(o.id) && o.id == id).ToList();
            }
            
            return list;
        }

        [HttpPost]
        public IActionResult Create(string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowCreate = User.HasClaim(
                    c => c.Type == ClaimTypes.Role
                    && c.Value == "ClientAdmin");
            var isAdmin = User.HasClaim(
                    c => c.Type == ClaimTypes.Role
                    && c.Value == "Admin");

            if ((_db.v_active_licences.Where(l => l.id == currentUserID).Select(l => l.o_avail).FirstOrDefault() > 0) && (allowCreate))
            {
                // есть возможность создать объект
                var newRow = new ObjectControl();
                if (values != null)
                {
                    JsonConvert.PopulateObject(values, newRow);
                    _db.ObjectControl.Add(newRow);
                    _db.SaveChanges();
                }

                var link = new user_objects_ref();
                link.idm_object = newRow.id;
                link.idm_user = currentUserID;
                _db.user_objects_ref.Add(link);
                _db.SaveChanges();

                return Ok(newRow);
            } else
            {
                // лимит на объекты исчерпан
                return BadRequest(new { msg = "Лимит объектов для вашей учетной записи исчерпан"});
            }

            

            
        }


        [HttpPut]
        public IActionResult Update(int key, string values)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();            
            var allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();

            if (allowed_objects.Contains(key))
            {
                var newData = _db.ObjectControl.First(o => allowed_objects.Contains(o.id) && o.id == key);
                JsonConvert.PopulateObject(values, newData);
                _db.SaveChanges();
                return Ok(newData);
            } else
            {
                return BadRequest(new { msg = "Ошибка редактирования объекта" });
            }
            
        }


        [HttpDelete]
        public void Delete(int key)
        {
            var currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
            var allowed_objects = _db.user_objects_ref.Where(u => u.idm_user == currentUserID).Select(u => u.idm_object).ToList();

            if (allowed_objects.Contains(key))
            {
                var row = _db.ObjectControl.First(o => allowed_objects.Contains(o.id) && o.id == key) ;
                _db.ObjectControl.Remove(row);
                _db.SaveChanges();
                //return Ok(newData);
            }
            else
            {
                //return BadRequest(new { msg = "Ошибка редактирования объекта" });
            }

            
        }

        internal List<int> GetUserObjects(string? userId)
        {

            //string currentUserID = "";
            //if (User != null)
            //{
            //    currentUserID = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault();
                
            //}

            //var currentUserID = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            //var currentUserID = "cd142210-06a1-4803-84aa-7db50269a0a7";
            


            List<int> objects = new List<int>();

            if (userId == null)
            {
                objects = _db.ObjectControl.Select(o => o.id).ToList();
            }
            else
            {

                objects = _db.user_objects_ref.Where(o => o.idm_user == userId).Select(o => o.idm_object).ToList();
            }

            return objects;
        }


    }
}
