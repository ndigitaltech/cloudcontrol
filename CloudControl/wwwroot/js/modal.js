class Modal {
    constructor(name) {
        if(!name) return;
        this.create(name);
    }
    create(name){
        if(!name) return;
        this.modal =  document.querySelector(".modal[data-name='" + name + "']");

        let closeButtons = this.modal.querySelectorAll('[data-close]');
        if(closeButtons && closeButtons.length){
            closeButtons.forEach(b => {
                b.addEventListener('click', () => this.close())
            })
        }

        let overlay = this.modal.querySelector('.modal__overlay');
        if(overlay)
            overlay.onclick = () => confirm('Вы уверены, что хотите закрыть текущее окно?') ? this.close() : '';

    }
    open(modalName){
        this.modal.classList.add('modal-opened');
        document.body.classList.add('noscroll');
    }
    close(){
        this.modal.classList.remove('modal-opened');
        document.body.classList.remove('noscroll');
    }
}

window.Modal = Modal;