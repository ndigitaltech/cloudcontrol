class Scroll {
	disable() {
		document.body.classList.add('noscroll');
		return !this.isEnabled();
	}
	enable() {
		document.body.classList.remove('noscroll');
		return this.isEnabled();
	}
	toggle() {
		document.body.classList.toggle('noscroll');
		return this.isEnabled();
	}
	isEnabled(){
		return document.body.classList.contains('noscroll');
	}
}
window.scroll = new Scroll();