document.addEventListener('DOMContentLoaded', e => {



    try {
        new Drop({
            root:'.drop',
            toggleClass:'--open'
        });
    }
    catch (e) {
        console.error(e);
    }

    try {
        document.querySelectorAll('[data-modal-open]').forEach(button => button.addEventListener('click', e => {
            (new Modal(button.getAttribute('data-modal-open'))).open();
        }))
    }catch (e) {
        console.error(e);
    }

    try {
        document.querySelectorAll('[data-modal-close]').forEach(button => button.addEventListener('click', e => {
            (new Modal(button.getAttribute('data-modal-close'))).close();
        }))
    }catch (e) {
        console.error(e);
    }


    try {
        document.querySelectorAll('[data-copy]').forEach(element => {
            let input = element.querySelector('input');
            element.addEventListener('click', e => {
                fallbackCopyTextToClipboard(input.value);
                element.classList.add('copied');
                setTimeout(e => {
                    element.classList.remove('copied');
                },2000)
            })

        })
    }catch (e) {
        console.error(e);
    }



    try {

        let ExampleModal = new Modal('example-1');

        document.querySelectorAll('form.prevent').forEach(form => {

            let submit = form.querySelector('[type="submit"]');
            let redirect = form.getAttribute('data-redirect');

            /**
             * Название окошка для закрытия после успешного сабмита
             * @type {string}
             */
            let modalName = form.getAttribute('data-close-after');

            form.addEventListener('submit', e => {
                e.preventDefault();



                /**
                 * formData можно вставлять в тело пост запроса
                 * @type {FormData}
                 */
                let formData = new FormData(form);



                /**
                 * Вот так можно получит поля формы как массив, но у всех инпутов должен быть атрибут name
                 * @type {any}
                 */
                let data = Object.fromEntries(new FormData(form));
                console.log(data);



                /**
                 * Добавляем анимацию на кнопку сабмита
                 * и выключаем, чтоб не накликали
                 */
                submit.setAttribute('disabled', true);
                submit.classList.add('--is-loading');



                /**
                 * Эмуляция загрузки
                 */
                setTimeout(e => {
                    submit.removeAttribute('disabled');
                    submit.classList.remove('--is-loading');
                    setTimeout(e => {

                        if(redirect) location.href = redirect;

                        if(modalName) (new Modal(modalName)).close();
                        else ExampleModal.close();


                    }, 100)
                }, 2000)

            })

        })
    }catch (e) {
        console.error(e);
    }




    /**
     * Можно так, если нужна переменная, и потом окрываешь или закрываешь
     */

    window.exampleModal = new Modal('example-1');

    /**
     * либо так
     * (new Modal('example-1')).close();
     */



})

