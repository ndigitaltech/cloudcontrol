﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CloudControl.Models;

namespace CloudControl.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<devices> devices { get; set; }
        public DbSet<ObjectControl> ObjectControl { get; set; }
        public DbSet<PrioritiesTimeouts> PrioritiesTimeouts { get; set; }
        public DbSet<recipients> recipients { get; set; }
        public DbSet<services> services { get; set; }
        public DbSet<devices_type> devices_type { get; set; }
        public DbSet<link_devices_priorities> link_devices_priorities { get; set; }
        public DbSet<alarms> alarms { get; set; }
        public DbSet<v_open_alarms> v_open_alarms { get; set; }
        public DbSet<v_closed_alarms> v_closed_alarms { get; set; }
        public DbSet<v_alarms_by_objects> v_alarms_by_objects { get; set; }
        public DbSet<v_alarms_count_by_objects> v_alarms_count_by_objects { get; set; }
        public DbSet<v_alarms_all_by_status> v_alarms_all_by_status { get; set; }
        public DbSet<user_objects_ref> user_objects_ref { get; set; }
        public DbSet<user_devices_ref> user_devices_ref { get; set; }
        public DbSet<recipient_objects_ref> recipient_objects_ref { get; set; }
        public DbSet<organisations> organisations { get; set; }
        public DbSet<mail_channels> mail_channels { get; set; }
        public DbSet<mail_messages> mail_messages { get; set; }
        public DbSet<v_active_licences> v_active_licences { get; set; }

        //public DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<AspNetUserRoles>().HasKey(rpr => new { rpr.RoleId, rpr.UserId});

        //    //base.OnModelCreating(modelBuilder);


        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<v_open_alarms>().HasNoKey();
            modelBuilder.Entity<v_closed_alarms>().HasNoKey();
            modelBuilder.Entity<v_alarms_by_objects>().HasNoKey();
            modelBuilder.Entity<v_alarms_count_by_objects>().HasNoKey();
            modelBuilder.Entity<v_alarms_all_by_status>().HasNoKey();
            modelBuilder.Entity<user_objects_ref>().HasKey(u => new { u.idm_object,u.idm_user} );
            modelBuilder.Entity<user_devices_ref>().HasKey(u => new { u.idm_device,u.idm_user} );
            modelBuilder.Entity<recipient_objects_ref>().HasKey(u => new { u.idm_object,u.idm_recipient} );
            modelBuilder.Entity<mail_messages>().HasKey(u => u.id );
            modelBuilder.Entity<mail_channels>().HasKey(u => u.id );
            modelBuilder.Entity<v_active_licences>().HasKey(u => u.id );
            modelBuilder.Entity<devices>().HasKey(u => u.id );
            //modelBuilder.Entity<devices>().Property(u => u.is_enable ).HasColumnType("bool");
            base.OnModelCreating(modelBuilder);
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
    }
}
