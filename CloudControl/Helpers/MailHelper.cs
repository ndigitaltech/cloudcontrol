﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudControl.Models;
using CloudControl.Data;

namespace CloudControl.Helpers
{
    public class MailHelper
    {
        
        private ApplicationDbContext _db;

        public MailHelper(ApplicationDbContext db)
        {
            _db = db;
        }
        

        /// <summary>
        /// Отправка сообщения о подтверждении учетной записи в очередь
        /// </summary>
        /// <param name="uid">id пользователя</param>
        /// <param name="updateId">id uploaded_files</param>
        /// <param name="idm_channel">id канала</param>
        internal void SendConfirm(string uid,string title, string body, int idm_channel = 1)
        {
            //var request = _db.v_requests_list_of_visits.FirstOrDefault(v => v.id == rid && v.is_top == 1);
            var user = _db.Users.Where(u => u.Id == uid).FirstOrDefault();

            var mail_to = user.Email;
            var mail_row = new mail_messages()
            {
                idm_channel = idm_channel,
                mail_to = mail_to,
                mail_title = title,
                mail_body = body                
            };
            _db.mail_messages.Add(mail_row);
            _db.SaveChanges();
        }
    }
}
